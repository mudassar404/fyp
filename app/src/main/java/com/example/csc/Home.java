package com.example.csc;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.example.csc.Model.Car;
import com.example.csc.Model.Items;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.squareup.picasso.Picasso;

public class Home extends AppCompatActivity {
    RecyclerView recyclerView;

    FloatingActionButton floatingActionButton, floatingActionButton2;
    FirebaseRecyclerOptions<Items> options;
    FirebaseRecyclerAdapter<Items, MyViewHolder> adapter;
    EditText inputSearch;
    DatabaseReference DataRef;

    FirebaseAuth firebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        floatingActionButton = (FloatingActionButton) findViewById(R.id.floating_button);
        floatingActionButton2 = (FloatingActionButton) findViewById(R.id.floating_button2);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 2));
        inputSearch = (EditText) findViewById(R.id.inputSearch);
        recyclerView.setHasFixedSize(true);
      // recyclerView.setNestedScrollingEnabled(false);
        firebaseAuth = FirebaseAuth.getInstance();
        DataRef = FirebaseDatabase.getInstance().getReference().child("Items");

        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (firebaseAuth.getCurrentUser() != null && firebaseAuth.getCurrentUser().isEmailVerified()) {
                    ////main is here where logged in user upload his data
                    startActivity(new Intent(Home.this, MainActivity.class));
                } else {
                    startActivity(new Intent(Home.this, SignIn.class));
                }

            }
        });
        floatingActionButton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                startActivity(getIntent());
            }
        });
        LoadData("");
        inputSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }
            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.toString() != null) {
                    LoadData(editable.toString());
                } else {
                    LoadData("");
                }
            }
        });
    }

    private void LoadData(String data) {

        Query query = DataRef.orderByChild("CarName").startAt(data).endAt(data + "\uf8ff");

        options = new FirebaseRecyclerOptions.Builder<Items>().setQuery(query, Items.class).build();

        adapter = new FirebaseRecyclerAdapter<Items, MyViewHolder>(options) {
            @Override
            protected void onBindViewHolder(@NonNull MyViewHolder holder, final int position, @NonNull Items model) {
                recyclerView.smoothScrollToPosition(position);
                holder.textView_card.setText(model.getCarName() + "$");
                Picasso.get().load(model.getImageUri()).into(holder.imageView_card);
                holder.v.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                     /*   Intent intent=new Intent(Home.this,Test.class);
                        intent.putExtra(Test.currentrefrence,getRef(position).getKey());
                        startActivity(intent);
*/
                        // Toast.makeText(getApplicationContext(), "sdasdsa" + position, Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(Home.this, Item_Detail.class);
                        intent.putExtra(Item_Detail.ItemId, getRef(position).getKey());
                        startActivity(intent);

                    }
                });
            }

            @NonNull
            @Override
            public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view, parent, false);
                return new MyViewHolder(view);
            }
        };
        adapter.startListening();
        recyclerView.setAdapter(adapter);
        // adapter.notifyDataSetChanged();

    }

}