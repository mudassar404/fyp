package com.example.csc.Model;

public class Car {
    private String CarName;
    private String ImageUri;
    private String ItemPrice;
    private String ItemDescription;

    public Car() {
    }

    public Car(String carName, String imageUri, String itemPrice, String itemDescription) {
        CarName = carName;
        ImageUri = imageUri;
        ItemPrice = itemPrice;
        ItemDescription = itemDescription;
    }


    public String getItemDescription() {
        return ItemDescription;
    }

    public String getItemPrice() {
        return ItemPrice;
    }

    public String getItemprice() {
        return ItemPrice;
    }


    public void setItemprice(String itemprice) {
        ItemPrice = itemprice;
    }

    public String getCarName() {
        return CarName;
    }

    public void setCarName(String carName) {
        CarName = carName;
    }

    public String getImageUri() {
        return ImageUri;
    }

    public void setImageUri(String imageUri) {
        ImageUri = imageUri;
    }
}
