package com.example.csc.Model;

public class Items {
    private String CarName;
    private String ImageUri;
    private String ItemPrice;
    private String ItemDescription;


    public Items() {
    }

    public Items(String carName, String imageUri, String itemPrice, String itemDescription) {
        CarName = carName;
        ImageUri = imageUri;
        ItemPrice = itemPrice;
        ItemDescription = itemDescription;
    }


    public String getCarName() {
        return CarName;
    }

    public void setCarName(String carName) {
        CarName = carName;
    }

    public String getImageUri() {
        return ImageUri;
    }

    public void setImageUri(String imageUri) {
        ImageUri = imageUri;
    }

    public String getItemPrice() {
        return ItemPrice;
    }

    public void setItemPrice(String itemPrice) {
        ItemPrice = itemPrice;
    }

    public String getItemDescription() {
        return ItemDescription;
    }

    public void setItemDescription(String itemDescription) {
        ItemDescription = itemDescription;
    }
}
