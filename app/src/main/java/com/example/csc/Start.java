package com.example.csc;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.csc.freelancing.FreeLancingLoginActivity;

public class Start extends AppCompatActivity {
    Button sell_note_main,get_work_main;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);

        sell_note_main=(Button)findViewById(R.id.sell_notes_main);
        get_work_main=(Button)findViewById(R.id.get_work_main);


        ////This method will Start the Olx?Selling Module///////////////
        sell_note_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(Start.this,Home.class);
                startActivity(intent);
              //  startActivity(new Intent(Start.this,Home.class));
            }
        });


        ////////////This method will start the freelancing module//////////////
        get_work_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              ///  startActivity(new Intent(MainActivity.this,Freelancing_Activity.class));
                startActivity(new Intent(Start.this, FreeLancingLoginActivity.class));
            }
        });


    }


}